# slideSegmenter

slideSegmenter is a MATLAB UI designed to easily allow for manual segmentation
of images acquired using the Perkin Elmer Operetta scanner. 

This includes defining arbitrary polygonal areas within an arbitrary number of 
brain slices, as well as the pial and white matter borders of the cortex. 

Defining the pia and white matter borders allows for calculation of absolute 
and normalised cortical depth

To start, add the slideSegmenter directory to your MATLAB path, and run
    sliderSegmenter.gui

Segmentations can then be exported in the YAML format, and used to segment cells
using the segmentCellPostions script
