
%% Get cell positions and segmentation - this function does all the work, the rest is for show.
[cp, sliceIdx, segLabels, rV] = slideSegmenter.segmentCellPositions();


%% Prepare figure
figure;hold on
cols=hsv(numel(sliceIdx));
%% Display segmented cells
for ii=1:numel(sliceIdx)
segmentedIdx = find(cp.segmentationRegions(:, ii) & cp.segmentationStatus == 1);
    
  scatter(cp.PositionX_m_adjusted(segmentedIdx)/1000, ...
            cp.PositionY_m_adjusted(segmentedIdx)/1000, ...
            [], ...
            cols(ii, :));
   if ~isempty(segmentedIdx) && (sum(isnan(cp.normalisedDepth(segmentedIdx))) / numel(segmentedIdx)) < 0.1 %show if most cells (>90%) are cortical
       h=gcf;
       figure
       scatter(cp.PositionX_m_adjusted(segmentedIdx), cp.PositionY_m_adjusted(segmentedIdx), [], cp.normalisedDepth(segmentedIdx), 'filled')
       colorbar
       title([segLabels{ii} ' Normalised Depth'], 'Interpreter', 'none');
       
       figure
       scatter(cp.PositionX_m_adjusted(segmentedIdx), cp.PositionY_m_adjusted(segmentedIdx), [], cp.absoluteDepth(segmentedIdx), 'filled')
       colorbar
       title([segLabels{ii} ' Absolute Depth'], 'Interpreter', 'none');
       figure(h)
       clear h
   end
end
%% Display unsegmented cells
noSeg = ~sum(cp.segmentationRegions, 2);

scatter(cp.PositionX_m_adjusted(noSeg)/1000, ...
        cp.PositionY_m_adjusted(noSeg)/1000, ...
        [], ...
        [ 0 0 0], ...
        'x')
%% Display multiply segmented cells
multipSeg = cp.segmentationStatus == 2;

scatter(cp.PositionX_m_adjusted(multipSeg)/1000, ...
        cp.PositionY_m_adjusted(multipSeg)/1000, ...
        [], ...
        [ 0.2 0.2 0.2], ...
        '*')
%% Show legend

legend([strcat('Slice', cellfun(@num2str, num2cell(sliceIdx)), '_', segLabels); {'Unsegmented'; 'Multiple Segmentation'}], 'Interpreter', 'none')

%% Display segmentations
for ii=1:numel(sliceIdx)
    patch(rV{ii,1}, rV{ii,2}, 0, 'FaceColor', 'none', 'EdgeColor', cols(ii, :))
end


%% Clean up

hold off
axis equal
clear ii cols noSeg segmentedIdx

% clear