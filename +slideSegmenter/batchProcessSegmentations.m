function batchProcessSegmentations(specFile)

    %% Input Parsing
    if nargin < 1 || ~exist(specFile, 'file')
        specFile=uigetfile('*.*', 'Please select batch processing file');
    end


    %% Open File
    fh=fopen(specFile, 'r');

    %% Prepare
    entries = cell(0,4);
    ii=1;

    %% Read CSV entries in the format location_data, segmentationYML, offsetFile, outputFile
    while ~feof(fh)
        nextLine = fgetl(fh);
        if isempty(nextLine)
            continue
        end
        splitLine = strsplit(nextLine, ',');
        entries(ii, :)= splitLine;
        ii=ii+1;
    end

    %% Close batch processing file
    fclose(fh);

    %% Execute segmentations
    for ii=1:size(entries,1)
        processIndividualSegmentation(entries{ii, 1}, entries{ii, 2}, entries{ii, 3}, entries{ii, 4})
    end

end

function processIndividualSegmentation(pathToCellLocationData, pathToSegmentationYAML, pathToOffsetFile, outputFilePath)

    cp=slideSegmenter.segmentCellPositions(pathToCellLocationData, pathToSegmentationYAML, pathToOffsetFile);
    
    writetable(cp, outputFilePath);

end