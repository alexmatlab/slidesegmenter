function Iout=adjustLimits(I, contrastLimits)
    I=single(I);
    Iout = (I-contrastLimits(2))*1/(contrastLimits(2)-contrastLimits(1))+1;
end
