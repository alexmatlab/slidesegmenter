function T=read40xData(pathToFile)
   warning off
   T=readtable(pathToFile);
   warning on
   T.Properties.VariableNames = getHeaderNames(T, T.Properties.VariableNames);
end

function suggestedHeaders = getHeaderNames(T, suggestedHeaders)
    %% Rename specific variables to remove ambiguity
    suggestedHeaders{strcmpi(suggestedHeaders, 'row')}='TileRow';
    suggestedHeaders{strcmpi(suggestedHeaders, 'column')}='TileColumn';
    
    %% Remove ugly bits
    suggestedHeaders = cellfun(@(x) strrep(x, '__', '_'), suggestedHeaders, 'UniformOutput', false);
    suggestedHeaders = cellfun(@(x) strrep(x, '__', '_'), suggestedHeaders, 'UniformOutput', false);
    suggestedHeaders = cellfun(@removeFinalUnderscore, suggestedHeaders, 'UniformOutput', false);
    
end

function strOut = removeFinalUnderscore(strIn)
     if strIn(end)=='_'
        strOut = removeFinalUnderscore(strIn(1:end-1));
    else
        strOut = strIn;
    end
end