classdef slice < handle
    properties
        name
        rois
        roiNames = {}
        wmBorder
        pialSurface
    end
    
    properties(Dependent, SetAccess=protected)
       piaDefined
       wmDefined
    end
    
    methods
        %% Constructor
        function obj=slice(nm, structData, axisToRecreateIn)
            obj.name=nm;
            if nargin > 1
                obj.recreateFromStruct(structData, axisToRecreateIn);
            end
        end
        %% Add / delete
        function addROI(obj, newRoi, newRoiName)
            
            if isempty(obj.rois)
                obj.rois = newRoi;
            else
                obj.rois = [obj.rois newRoi];
            end

            newName = matlab.lang.makeUniqueStrings(newRoiName, obj.roiNames); %ensure the name is unique if the user accidentally names two regions duplicated

            obj.roiNames{end+1} = newName;
            
        end
        
        function deleteROI(obj, idx)
            delete(obj.rois(idx))
            obj.rois(idx)=[];
            obj.roiNames(idx)=[];
        end
        
        %% Surface

        function setBorder(obj, parent, src, type)
            switch type
                case 'pia'
                    if obj.piaDefined
                        response=questdlg('Surface already defined. Do you wish to re-draw?', 'Pial Surface Marking', 'Yes', 'Cancel', 'Cancel');
                        if ~strcmpi(response, 'yes')
                            return
                        end
                        delete(obj.pialSurface)
                    end
                    
                    h=impoly(parent.hSlideAxis, 'Closed', false);
                    h.Deletable=0;
                    h.setColor([1 1 1])
                    
                    obj.pialSurface=h;
                case 'wm'
                     if obj.wmDefined
                        response=questdlg('Surface already defined. Do you wish to re-draw?', 'WM Border Marking', 'Yes', 'Cancel', 'Cancel');
                        if ~strcmpi(response, 'yes')
                            return
                        end
                        delete(obj.wmBorder)
                    end
                    
                    h=impoly(parent.hSlideAxis, 'Closed', false);
                    h.Deletable=0;
                    h.setColor([1 1 1])
                    
                    obj.wmBorder=h;
                otherwise
                    error('unknown border type')
            end
            obj.setButtonState(src, 'set')
        end
        
        
        function setBorderState(obj, borderMode)
            
            switch borderMode
                case 'active'
                    col=[1 1 1];
                case 'off'
                    col = [0.4 0.4 0.4];
                otherwise
                    error('unknown state')
            end
            if obj.piaDefined
                obj.pialSurface.setColor(col)
            end
            if obj.wmDefined
                obj.wmBorder.setColor(col)
            end
                    
        end
        %% Getters
        function pia=get.piaDefined(obj)
            pia=~isempty(obj.pialSurface);
        end
        
        function wm=get.wmDefined(obj)
            wm=~isempty(obj.wmBorder);
        end
        
        %% Data export
        function S=toStruct(obj)
            S=struct;
            if obj.piaDefined
                S.pia=obj.pialSurface.getPosition;
            else
                S.pia=[];
            end
            if obj.wmDefined
                S.wm=obj.wmBorder.getPosition;
            else
                S.wm=[];
            end
            if isempty(obj.rois)
                S.rois=[];
            else
                for ii=1:numel(obj.rois)
                    S.rois(ii).name = obj.roiNames{ii};
                    S.rois(ii).position = obj.rois(ii).getPosition;
                end
            end
        end
        
        function recreateFromStruct(obj, structData, axisToRecreateIn)
            
            for ii=1:numel(structData.rois)
                coords = str2num(structData.rois(ii).position{1}); %#ok<*ST2NM>
                if ~isempty(coords)
                    newRoi = impoly(axisToRecreateIn, coords);
                    obj.addROI(newRoi, structData.rois(ii).name)
                end
            end
            
            %% Pia / WM
            if ~isempty(structData.wm)
                obj.wmBorder = impoly(axisToRecreateIn, str2num(structData.wm{1}), 'Closed', false);
            end
            if ~isempty(structData.pia)
                obj.pialSurface = impoly(axisToRecreateIn, str2num(structData.pia{1}), 'Closed', false);
            end
        end
    end
    
    methods(Static)
        function setButtonState(button, state)
            switch state
                case 'set'
                    button.ForegroundColor=[1 1 1];
                    button.BackgroundColor = [0.2 0.2 0.2];
                case 'ready'
                    button.ForegroundColor=[0 0 0];
                    button.BackgroundColor = [0.94 0.94 0.94];
            end
        end
    end
end