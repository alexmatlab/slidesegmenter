function [cellPositions, sliceIdx, segmentationLabels, roiVertices] = segmentCellPositions(pathToCellLocationData, pathToSegmentationYAML, pathToOffsetFile)

    %% Input parsing
    
    if nargin < 3 
        response = questdlg('Do you want to adjust cell positions using manual offset?', 'slideSegmenter', 'Yes', 'No', 'Yes');
        if strcmp(response, 'Yes')
            [f,p] = uigetfile({'*.txt'; '*.*'}, 'Select Cell Position Offset File');
            pathToOffsetFile = fullfile(p,f);
        else
            pathToOffsetFile = [];
        end
    end
    
    if exist(pathToOffsetFile, 'file')
        offsets = dlmread(pathToOffsetFile);
    else
        offsets=[0 0];
    end
    
    if nargin < 2 || isempty(pathToSegmentationYAML)
        [f,p] = uigetfile({'*.yml'; '*.*'}, 'Select Segmentation file');
        pathToSegmentationYAML = fullfile(p,f);
    end
    
    if nargin < 1 || isempty(pathToCellLocationData)
        [f,p] = uigetfile({'*.txt'; '*.*'}, 'Select Cell Location txt file');
        pathToCellLocationData = fullfile(p,f);
    end

    
    %% Prepare data structures
    
    segmentations = slideSegmenter.YAML.read(pathToSegmentationYAML);
    nRois = countRois(segmentations);

    cellPositions=slideSegmenter.utils.read40xData(pathToCellLocationData);
    cellPositions.segmentationRegions=false(size(cellPositions, 1), nRois);

    
    sliceIdx = nan(nRois,1);
    segmentationLabels=cell(nRois, 1);
    roiVertices=cell(nRois,2);
    ii=0;
    
    %% Adjust cell positions
    cellPositions.PositionX_m_adjusted = cellPositions.PositionX_m/1000 + offsets(1);
    cellPositions.PositionY_m_adjusted = cellPositions.PositionY_m/1000 + offsets(2);
    
    %% Add in a field for depth from pia, and one for normalised cortical depth
    cellPositions.absoluteDepth = nan(size(cellPositions, 1), 1);
    cellPositions.normalisedDepth = nan(size(cellPositions, 1), 1);
    
    %% Loop through and assign
    for ss=1:numel(segmentations.slices)
        thisSlice = segmentations.slices(ss);
        for rr=1:numel(thisSlice.rois)
            thisRoi = thisSlice.rois(rr);
            
            ii=ii+1;
            segmentationLabels{ii} = thisRoi.name; 
            sliceIdx(ii)=ss;
            
            [X,Y]=positionCellToVertexXY(thisRoi.position);
            
            roiVertices{ii,1} = X/1000;
            roiVertices{ii,2} = Y/1000;
            
            cellsWithinThisROI = (inpolygon(cellPositions.PositionX_m_adjusted, ...
                                            cellPositions.PositionY_m_adjusted, ...
                                            X, ...
                                            Y));
            
            cellPositions.segmentationRegions(cellsWithinThisROI, ii) = true;
        end
        %% Now calculate cortical depth
        cortical = getCorticalCells(cellPositions.PositionX_m_adjusted, cellPositions.PositionY_m_adjusted, thisSlice);
        
        if any(cortical)
            pialDistance = findMinimumDistanceToBorder(cellPositions.PositionX_m_adjusted(cortical), cellPositions.PositionY_m_adjusted(cortical), str2num(thisSlice.pia{1}));
            wmDistance   = findMinimumDistanceToBorder(cellPositions.PositionX_m_adjusted(cortical), cellPositions.PositionY_m_adjusted(cortical), str2num(thisSlice.wm{1}));
            
            normalisedDistance = pialDistance ./ (pialDistance + wmDistance);
            
            cellPositions.absoluteDepth(cortical) = pialDistance;
            cellPositions.normalisedDepth(cortical) = normalisedDistance;
        end
    end
    
    %% Finally - assign a single segmentation region based on the segmentation
      % If a cell falls in to multiple regions, give it the label
      % 'MULTIPLE'
      
      cellPositions.segmentation = cell(size(cellPositions, 1), 1);
      cellPositions.segmentation(:) = {''};
      for ii=1:numel(segmentationLabels)
          cellPositions.segmentation(cellPositions.segmentationRegions(:, ii)==1) = segmentationLabels(ii);
      end
      
      cellPositions.segmentation(sum(cellPositions.segmentationRegions, 2) > 1) = {'MULTIPLE'};
      %% Convenience function - segmentation status. 0 means not segmented, 1 is segmented, 2 is multiply segmented
      cellPositions.segmentationStatus = zeros(size(cellPositions, 1), 1);
      cellPositions.segmentationStatus(sum(cellPositions.segmentationRegions, 2) == 1) = 1;
      cellPositions.segmentationStatus(sum(cellPositions.segmentationRegions, 2) > 1) = 2;
end

function [X,Y] = positionCellToVertexXY(position)
    coordPairs = strsplit(position{1}, ';');

    X=zeros(numel(coordPairs), 1);
    Y=zeros(numel(coordPairs), 1);

    for ii=1:numel(coordPairs)
        xy = cellfun(@str2num, strsplit(coordPairs{ii}));
        X(ii)=xy(1);
        Y(ii)=xy(2);
    end
end

function nRois = countRois(segmentations)
    nRois = 0;
    
    for ss=1:numel(segmentations.slices)
        thisSlice = segmentations.slices(ss);
        for rr=1:numel(thisSlice.rois)
            nRois = nRois + 1;
        end
    end
end

function cortical = getCorticalCells(X,Y,slice)
    cortical = false(size(X));
    if ~isempty(slice.pia) && ~isempty(slice.wm)
        pia = str2num(slice.pia{1}); %#ok<ST2NM>
        wm = str2num(slice.wm{1}); %#ok<ST2NM>
        
        if eucdist(pia(1, :), wm(1, :)) < eucdist(pia(1, :), wm(end, :))
            ctx = [pia; flipud(wm)];
        else
            ctx = [pia; wm];
        end
        
        cortical = inpolygon(X,Y,ctx(:, 1), ctx(:, 2));
    end
end

function d=eucdist(x,y)
    d = sqrt(sum((x-y).^2));
end

function d=findMinimumDistanceToBorder(X, Y, border)
    % Finds the distance from cells with position x and y to the polygon
    % defined border by testing to each line segment and then finding the
    % minimum
    
    nSegments = size(border, 1)-1;
    
    D = zeros(numel(X), nSegments);
    
    for ii=1:nSegments
        for pp=1:numel(X)
            D(pp, ii) = slideSegmenter.utils.distanceToLineSegment(border(ii, :), border(ii+1, :), [X(pp) Y(pp)]);
        end
    end
    
    d=min(D, [], 2);
end



