classdef gui<handle
    %GUI GUI for interactively defining ROIs on slides from the Perkin
    %Elmer Operetta system
    
    %% Properties
    properties(SetAccess=protected)
        tileSizePixels = [1080 1080];
        pixelSize=2.3919043238270468e-06; % from the image metadata (in metres)
        
        baseDirectory
        
        slices = slideSegmenter.utils.slice('1')
        
        channelNames
        tileFileNames
        
        XYPosition
        Images
        
    end
    
    properties(Access=protected)
        
        %% UI Elements
        
        hDisplayPanel
        
        hROIPanel
        hSlideName
        hSliceName
        hSliceList
        hROIList
        hROIDelete
        
        hCheckch1
        hCheckch2
        
        hMinch1
        hMaxch1
        hMinch2
        hMaxch2
        
        hCh1Selection
        hCh2Selection
        
        hDefinePia
        hDefineWM
        
        hImageHandles
        
        cellLocations
        hCellLocationDisplay
        
        overlayXOffset
        overlayYOffset
        
        %% Contrast scaled images
        ch1_scaled
        ch2_scaled
        
        %% Internal tracking
        
        panxInt = 0
        panyInt = 0
    end
    
    properties(SetAccess=private, Dependent)
        currentSlice
        currentROI_idx
        currentROI
        
        ch1
        ch2
    end
    
    properties(SetAccess=protected)
        hMainFig
        hSlideAxis
    end
    
    %% Methods
    methods % Constructor
        function obj = gui(pathToBaseDirectory)
            
            
            %% Set up UI elements
            obj.hMainFig = figure(...
                            'Units', 'normalized', ...
                            'OuterPosition', [0.02 0.02 0.92 0.92], ...
                            'NumberTitle', 'off', ...
                            'Name', 'SlideSegmenter UI', ...
                            'MenuBar', 'none', ...
                            'ToolBar', 'figure', ...
                            'KeyPressFcn', {@hMainFig_Keypress, obj});
                        
            obj.hSlideAxis = axes(...
                            'Parent', obj.hMainFig, ...
                            'Units', 'normalized', ...
                            'Position', [0.02 0.03 0.6 0.94], ...
                            'Color', [0.2 0.2 0.2]);
           

                        
            
            setupDisplayPanel(obj);
            setupROIPanel(obj);
            setupOverlayPanel(obj);
            
            %% Load data
            
             if nargin < 1 
                 obj.baseDirectory = uigetdir([], '5x image tile directory');
             else
                  if isempty(pathToBaseDirectory) %allows testing mode
                      return
                  elseif exist(pathToBaseDirectory, 'dir')
                     obj.baseDirectory=pathToBaseDirectory;
                 else
                     error('Base directory does not appear to be a valid directory')
                 end
             end
             
           
            loadDataset(obj)
            
            %% Populate scaled data 
            obj.ch1_scaled=slideSegmenter.utils.adjustLimits(obj.ch1, [obj.hMinch1.UserData obj.hMaxch1.UserData]);
            obj.ch2_scaled=slideSegmenter.utils.adjustLimits(obj.ch2, [obj.hMinch2.UserData obj.hMaxch2.UserData]);
            
            %% Show 
            obj.setupTileImageObjects()
            obj.updateDisplay()
            axis(obj.hSlideAxis, 'equal')
        end
    end
    
    methods %Getters
        function sl=get.currentSlice(obj)
            sl=obj.slices(obj.hSliceList.Value);
        end
        
        function idx=get.currentROI_idx(obj)
            idx=obj.hROIList.Value;
        end
        
        function sl=get.currentROI(obj)
            sl=obj.currentSlice.rois(obj.currentROI_idx);
        end
        
        function I=get.ch1(obj)
            I=obj.Images{obj.hCh1Selection.Value};
        end
        
        function I=get.ch2(obj)
            I=obj.Images{obj.hCh2Selection.Value};
        end
            
    end
    
    methods(Access=protected) % UI setup
        function setupDisplayPanel(obj)
            obj.hDisplayPanel = uipanel(...
                'Parent', obj.hMainFig, ...
                'Units', 'normalized', ...
                'Position', [0.64 0.84 0.34 0.12], ...
                'Title', 'Display Options');

            %% ch1 Contrast
            obj.hMinch1 = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.45 0.55 0.1 0.25], ...
                'String', '0', ...
                'UserData', 0, ...
                'Callback', @(src, ~) contrastUpdate(obj, src));

            obj.hMaxch1 = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.56 0.55 0.1 0.25], ...
                'String', '5000', ...
                'UserData', 5000, ...
                'Callback', @(src, ~) contrastUpdate(obj, src));

            hAutoch1 = uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.73 0.55 0.1 0.25], ...
                'String', 'Auto', ...
                'Callback', @(~, ~) runAutoContrast(obj, 'ch1'));
    
            %% ch2 contrast

            obj.hMinch2 = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.45 0.15 0.1 0.25], ...
                'String', '0', ...
                'UserData', 0, ...
                'Callback', @(src, ~) contrastUpdate(obj, src));

            obj.hMaxch2 = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.56 0.15 0.1 0.25], ...
                'String', '5000', ...
                'UserData', 5000, ...
                'Callback', @(src, ~) contrastUpdate(obj, src));

            hAutoch2 = uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.73 0.15 0.1 0.25], ...
                'String', 'Auto', ...
                'Callback', @(~,~) runAutoContrast(obj, 'ch2'));
            
            %% Channel Selection
            obj.hCh1Selection = uicontrol(...
                'Style', 'popupmenu', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.25 0.45 0.15 0.35], ...
                'String', {''}, ...
                'Callback', @(src, ~) obj.channelSelectionChanged(src));
            
             obj.hCh2Selection = uicontrol(...
                'Style', 'popupmenu', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.25 0.05 0.15 0.35], ...
                'String', {''}, ...
                'Callback', @(src, ~) obj.channelSelectionChanged(src));
            
            %% Checkboxes (declared last for callback definition)
            obj.hCheckch1 = uicontrol(...
                'Style', 'Checkbox', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.05 0.5 0.1 0.35], ...
                'String', 'ch1', ...
                'Value', 1, ...
                'Callback', @(src, ~) setContrastControlState(obj, src, {hAutoch1, obj.hMinch1, obj.hMaxch1}));

            obj.hCheckch2 = uicontrol(...
                'Style', 'Checkbox', ...
                'Parent', obj.hDisplayPanel, ...
                'Units', 'normalized', ...
                'Position', [0.05 0.1 0.1 0.35], ...
                'String', 'ch2', ...
                'Value', 1, ...
                'Callback', @(src, ~) setContrastControlState(obj, src, {hAutoch2, obj.hMinch2, obj.hMaxch2}));

            end

        function setupROIPanel(obj)
            obj.hROIPanel = uipanel(...
                'Parent', obj.hMainFig, ...
                'Units', 'normalized', ...
                'Position', [0.64 0.22 0.34 0.60], ...
                'Title', 'ROI Management');

            %% Slide / slice naming
            htext=uicontrol(...
                'Style', 'text', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.02 0.92 0.1 0.04], ...
                'String', 'Slide ID:');

            jh = slideSegmenter.utils.findjobj(htext);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)

            obj.hSlideName = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.14 0.92 0.48 0.04], ...
                'Callback', @(~,~) obj.updateSliceList, ...
                'String', 'SLIDE_NAME');

            htext=uicontrol(...
                'Style', 'text', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.02 0.87 0.1 0.04], ...
                'String', 'Slice ID:');

            jh = slideSegmenter.utils.findjobj(htext);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)

            obj.hSliceName = uicontrol(...
                'Style', 'edit', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.14 0.87 0.15 0.04], ...
                'String', '1', ...
                'Enable', 'inactive');

            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.3 0.87 0.16 0.042], ...
                'String', 'Previous (<)', ...
                'Callback', @(~,~) incrementSliceNumber(obj, -1));
            
            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.47 0.87 0.15 0.042], ...
                'String', 'Next (>)', ...
                'Callback', @(~,~) incrementSliceNumber(obj, 1));
            
            %% Segmentation 
            
            uicontrol(...
                'Style', 'text', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.14 0.81 0.25 0.035], ...
                'String', 'Slices')
            
            obj.hSliceList = uicontrol(...
                'Style', 'list', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.14 0.03 0.3 0.78], ...
                'Callback', @(~,~) sliceListIndexChanged(obj));

            
            uicontrol(...
                'Style', 'text', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.46 0.81 0.25 0.035], ...
                'String', 'ROIs')
            
            obj.hROIList = uicontrol(...
                'Style', 'list', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.46 0.03 0.28 0.78], ...
                'Callback', @(~,~) ROIListIndexChanged(obj));


           
            
            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.75 0.768 0.23 0.042], ...
                'String', 'Add (a)', ...
                'Callback', @(~,~) addROI(obj));

            obj.hROIDelete = uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'Normalized', ...
                'Position', [0.75 0.718 0.23 0.042], ...
                'String', 'Delete (d)', ...
                'Enable', 'off', ...
                'Callback', @(~,~) deleteROI(obj));
            
            %% Pia / WM Definition
                        
            obj.hDefinePia =  uicontrol(...
                        'Style', 'pushbutton', ...
                        'Parent', obj.hROIPanel, ...
                        'Units', 'normalized', ...
                        'Position', [0.75 0.5 0.23 0.042], ...
                        'String', 'Define Pial Surface', ...
                        'Callback', @(src, ~) obj.currentSlice.setBorder(obj, src, 'pia'));
                         
            obj.hDefineWM = uicontrol(...
                        'Style', 'pushbutton', ...
                        'Parent', obj.hROIPanel, ...
                        'Units', 'normalized', ...
                        'Position', [0.75 0.45 0.23 0.035], ...
                        'String', 'Define WM Border', ...
                        'Callback', @(src, ~) obj.currentSlice.setBorder(obj, src, 'wm'));
                         
            %% Import / export buttons
            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.75 0.08 0.23 0.042], ...
                'String', 'Import Data', ...
                'Callback', @(~,~) obj.importData);
            
            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', obj.hROIPanel, ...
                'Units', 'normalized', ...
                'Position', [0.75 0.03 0.23 0.042], ...
                'String', 'Export Data', ...
                'Callback', @(~,~) obj.exportData);
            
            %% And finally update

            obj.updateSliceList();

        end
        
        function setupOverlayPanel(obj)
                       
            hCPOverlay = uipanel(...
                'Parent', obj.hMainFig, ...
                'Units', 'normalized', ...
                'Position', [0.64 0.02 0.34 0.18], ...
                'Title', 'Overlay Cell Positions');
            
            uicontrol(...
                'Style', 'Checkbox', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.05 0.5 0.5 0.35], ...
                'String', 'Display Cell Positions Overlaid', ...
                'Value', 0, ...
                'Callback', @(src, ~) obj.showHideCellPositions(src));
            
            obj.overlayXOffset = uicontrol(...
                'Style', 'edit', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.15 0.4 0.2 0.15], ...
                'String', '0', ...
                'Enable', 'off', ...
                'Callback', @(~,~) obj.adjustCellPosition());
            
            htext=uicontrol(...
                'Style', 'text', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.02 0.4 0.12 0.15], ...
                'String', 'X Offset:');

            jh = slideSegmenter.utils.findjobj(htext);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)

            obj.overlayYOffset = uicontrol(...
                'Style', 'edit', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.15 0.2 0.2 0.15], ...
                'String', '0', ...
                'Enable', 'off', ...
                'Callback', @(~,~) obj.adjustCellPosition());
            
            htext=uicontrol(...
                'Style', 'text', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.02 0.2 0.12 0.15], ...
                'String', 'Y Offset:');


            jh = slideSegmenter.utils.findjobj(htext);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)
            
            uicontrol(...
                'Style', 'pushbutton', ...
                'Parent', hCPOverlay, ...
                'Units', 'normalized', ...
                'Position', [0.67 0.05 0.3 0.15], ...
                'String', 'Export Offset Data...', ...
                'Enable', 'on', ...
                'Callback', @(~,~) obj.exportOffsetData());


        end
    end
    
    methods(Access=protected) % Metadata, position, and image loading functions

        function loadDataset(obj)
            %% Show wait dialog
            ssz=get(0, 'ScreenSize');
            hDlg = dialog('WindowStyle', 'Normal', 'Position', [ssz(3)/2-100 ssz(4)/2-20 200 40]);
            htext = uicontrol(...
                'Style', 'text', ...
                'Parent', hDlg, ...
                'Units', 'normalized', ...
                'Position', [0.02 0.02 0.96 0.96], ...
                'String', 'Please wait, data loading');

            jh = slideSegmenter.utils.findjobj(htext);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)
            set(hDlg, 'WindowStyle', 'Modal')

            %% Load 
            [channelXML, tileXML] = obj.getXMLMetaData(obj.baseDirectory);
            obj.parseChannelMetaData(channelXML);
            obj.parseTileMetaData(tileXML);
                        
            obj.XYPosition = obj.getTileXYPositionData(tileXML);
            
            obj.Images = cell(numel(obj.channelNames),1);
            for ii=1:numel(obj.Images)
                obj.Images{ii}=zeros(obj.tileSizePixels(1), obj.tileSizePixels(2), numel(obj.tileFileNames{ii}), 'uint16');
                for jj=1:numel(obj.tileFileNames{ii})
                    obj.Images{ii}(:,:,jj)=imread(fullfile(obj.baseDirectory, obj.tileFileNames{ii}{jj}));
                end
            end
%             obj.ch1 = obj.getTileImageData(obj.baseDirectory, obj.tileMetaData, 1);
%             obj.ch2 = obj.getTileImageData(obj.baseDirectory, obj.tileMetaData, 2);

            %% Get rid of loading dialog

            delete(hDlg);
        end
        
        function [channelXML, tileXML] = getXMLMetaData(~, baseDir)
            xmlFileName=fullfile(baseDir, 'Index.idx.xml');
            if ~exist(xmlFileName, 'file')
                error('Could not find index file at %s', xmlFileName)
            end
            xmlData=xml2struct(xmlFileName);
            
            channelXML = xmlData.Children(strcmp({xmlData.Children.Name}, 'Maps'));
            tileXML =  xmlData.Children(strcmp({xmlData.Children.Name}, 'Images'));
        end
        
        function parseChannelMetaData(obj, channelXML)
            
            nChannels = (numel(channelXML.Children(2).Children)-1)/2;
            
            obj.channelNames=cell(nChannels,1);
            
            for ii=1:nChannels
                data = channelXML.Children(2).Children(ii*2).Children(2).Children.Data;
                obj.channelNames(ii) = regexp(data, '(?<=ChannelName: )[\w \s]+(?=,)', 'match');
            end
            %% Set dimension of each tile
            dims = regexp(data, '(?<=Dims: )\[[\ ,\w\s]+\](?=,)', 'match');
            dims = str2num(dims{1}); 
            obj.tileSizePixels=dims;
            %% populate list boxes with available channels
            obj.hCh1Selection.String = obj.channelNames;
            obj.hCh1Selection.Value = 1;
            obj.hCh2Selection.String = obj.channelNames;
            obj.hCh2Selection.Value = numel(obj.channelNames);
        end
        
        function parseTileMetaData(obj, tileXML)
            
            nTiles  = (numel(tileXML.Children)-1)/2;
            channel =cell(nTiles,1);
            fileName    = cell(nTiles,1);
            
            for ii=1:nTiles
                c=getChildrenByName(tileXML.Children(ii*2), 'ChannelName');
                channel{ii} = c.Children.Data;
                f=getChildrenByName(tileXML.Children(ii*2), 'URL');
                fileName{ii}= f.Children.Data;
            end
            
            urls=cell(numel(obj.channelNames),1);
            for ii=1:numel(obj.channelNames)
                urls{ii}=fileName(strcmp(channel, obj.channelNames{ii}));
            end
            
            tilesPerChannel=cellfun(@numel, urls);

            if numel(unique(tilesPerChannel)) > 1
                error('Number of tiles not equal per channel')
            end
            
            obj.tileFileNames=urls;
            
        end

        function xy = getTileXYPositionData(obj, metaData)
            nTiles = (numel(obj.tileFileNames{1}));

            xy=zeros(nTiles, 2);

            for ii=1:nTiles
                skip = (ii-1)*numel(obj.channelNames)*2;
                
                xmlIdx=2+skip; %will always be the tile from the first channel
                
                x=getChildrenByName(metaData.Children(xmlIdx), 'PositionX');
                y=getChildrenByName(metaData.Children(xmlIdx), 'PositionY');
                xy(ii, 1) = str2double(x.Children.Data);
                xy(ii, 2) = str2double(y.Children.Data);
            end
        end


    end
    
    methods(Access=protected) % ROI and slice management
        
        function addROI(obj)
            %% Get name
            regionName = inputdlg('Region Name:', 'Slide Segmenter');
            if isempty(regionName) || (iscell(regionName) && isempty(regionName{1}))
                return
            end
            regionName = regionName{:};
            
            %% Get polygon
            h=impoly(obj.hSlideAxis);
            h.Deletable=0;

            %% Update internal data
            obj.currentSlice.addROI(h, regionName);

            %% Update list
            updateROIList(obj)
            obj.hROIList.Value = numel(obj.currentSlice.rois);
            obj.ROIListIndexChanged
        end

        function deleteROI(obj)
            currentRegionName = obj.hROIList.String{obj.currentROI_idx};
            response = questdlg(sprintf('Are you sure you want to delete region %s from slice %s?', currentRegionName, obj.currentSlice.name), 'Slide Segmenter', 'Yes', 'No', 'Yes');

            if strcmp(response, 'Yes')
                    obj.currentSlice.deleteROI(obj.currentROI_idx);
                    updateROIList(obj)      
                    if obj.currentROI_idx > numel(obj.currentSlice.rois)
                        obj.hROIList.Value = numel(obj.hROIList.String);
                    end
                    obj.ROIListIndexChanged
            end
        end

        function updateSliceList(obj)
            obj.hSliceList.String=strcat(obj.hSlideName.String, '_', {obj.slices.name});
        end
        
        function sliceListIndexChanged(obj)
            obj.updateROIList()
            if obj.currentSlice.piaDefined
                obj.currentSlice.setButtonState(obj.hDefinePia, 'set')
            else
                obj.currentSlice.setButtonState(obj.hDefinePia, 'ready')
            end
            
            if obj.currentSlice.wmDefined
                obj.currentSlice.setButtonState(obj.hDefineWM, 'set')
            else
                obj.currentSlice.setButtonState(obj.hDefineWM, 'ready')
            end
            
            for ii=1:numel(obj.slices)
                obj.slices(ii).setBorderState('off')
            end
            obj.currentSlice.setBorderState('active')
        end
        
        function updateROIList(obj)
            obj.hROIList.Value=1;
            obj.hROIList.String = obj.currentSlice.roiNames;
            obj.ROIListIndexChanged;
        end
        
        function ROIListIndexChanged(obj)
            
            sat=0.8;
            val=0.8;
            
            for ii=1:numel(obj.slices)
                for jj=1:numel(obj.slices(ii).rois)
                    obj.slices(ii).rois(jj).setColor(hsv2rgb([0.6 sat val]))
                end
            end
            for ii=1:numel(obj.currentSlice.rois)
                obj.currentSlice.rois(ii).setColor(hsv2rgb([0.3 sat val]))
            end

            if ~isempty(obj.currentSlice.rois)
                obj.currentROI.setColor(hsv2rgb([0 sat val]));
                obj.hROIDelete.Enable = 'on';
            else
                obj.hROIDelete.Enable = 'off';
            end

        end
    end
    
    methods(Access=protected) % UI callbacks
        
        function setContrastControlState(obj, hCheckBox, linkedControls)
            enabled=hCheckBox.Value;

            for ii=1:numel(linkedControls)
                if enabled
                    linkedControls{ii}.Enable = 'on';
                else
                    linkedControls{ii}.Enable = 'off';
                end
            end
            
            obj.updateDisplay()
        end
        
        function contrastUpdate(obj, src)
            
            oldValue=src.UserData;
            validateNumericInput(src);
            if src.UserData ~= oldValue %valid change
                switch src
                    case {obj.hMinch1 obj.hMaxch1}
                        obj.ch1_scaled=slideSegmenter.utils.adjustLimits(obj.ch1, [obj.hMinch1.UserData obj.hMaxch1.UserData]);
                    case {obj.hMinch2 obj.hMaxch2}
                        obj.ch2_scaled=slideSegmenter.utils.adjustLimits(obj.ch2, [obj.hMinch2.UserData obj.hMaxch2.UserData]);
                end
                
                obj.updateDisplay; %trigger refresh
            end
            
        end
        
        function runAutoContrast(obj, channel)
            set(gcf, 'Pointer', 'watch');drawnow
            switch channel
                case 'ch1'
                    if numel(obj.ch1) > 100000
                        idx=randperm(numel(obj.ch1), 10000);
                    else
                        idx=1:numel(obj.ch1);
                    end
                    lims=prctile(single(obj.ch1(idx)), [1 99]);
                    
                    obj.hMinch1.String=num2str(lims(1));   %Set minimum to new value. Don't trigger an update
                    obj.hMinch1.UserData=lims(1);
                    
                    obj.hMaxch1.String=num2str(lims(2));
                    obj.hMaxch1.UserData=lims(2)-1;    %Set userdata to a different value to ensure we trigger an update
                    
                    obj.contrastUpdate(obj.hMaxch1)    %Trigger update on maximum box
                    
                case 'ch2'
                    if numel(obj.ch2) > 100000
                        idx=randperm(numel(obj.ch2), 10000);
                    else
                        idx=1:numel(obj.ch2);
                    end
                    lims=prctile(single(obj.ch2(idx)), [1 99]);
                    
                    obj.hMinch2.String=num2str(lims(1));   %Set minimum to new value. Don't trigger an update
                    obj.hMinch2.UserData=lims(1);       
                    
                    obj.hMaxch2.String=num2str(lims(2));
                    obj.hMaxch2.UserData=lims(2)-1;     %Set userdata to a different value to ensure we trigger an update
                    
                    obj.contrastUpdate(obj.hMaxch2)     %Trigger update on maximum box
                otherwise
                    error('unknown channel')
            end
            set(gcf, 'Pointer', 'arrow')
        end
        
        function channelSelectionChanged(obj,src)
            switch src
                case obj.hCh1Selection
                    obj.hMaxch1.UserData = -1; %mark as dirty to force a refresh
                    obj.contrastUpdate(obj.hMaxch1);
                case obj.hCh2Selection
                    obj.hMaxch2.UserData = -1; %mark as dirty to force a refresh
                    obj.contrastUpdate(obj.hMaxch2);
            end
        end
    end
    
    methods % Display update
        function updateDisplay(obj)
            %% Display
            if obj.hCheckch1.Value && obj.hCheckch2.Value
                %% Set checkbox colour, refresh, and clear axes
                obj.hCheckch1.ForegroundColor=[0.8 0 0.8];
                obj.hCheckch2.ForegroundColor=[0 0.8 0];
                drawnow()
                %% Display truecolor image
                obj.displayTwoChannelOverlayImages;
            else
                %% Reset checkbox colours, refresh, and clear axes
                obj.hCheckch1.ForegroundColor=[0 0 0];
                obj.hCheckch2.ForegroundColor=[0 0 0];
                drawnow()
                
                %% Display
                if obj.hCheckch1.Value
                    obj.displaySingleChannelImages(obj.ch1_scaled)
                elseif obj.hCheckch2.Value
                    obj.displaySingleChannelImages(obj.ch2_scaled)
                end
                %% Set axes
                colormap gray
                set(obj.hSlideAxis, 'CLim', [0 1])
            end
        end
    end 
    
    methods(Access=protected) %Internal display functions
        function setupTileImageObjects(obj)
             %% Get limits
            [xLimits, yLimits]=getTileBaseLimits(obj);
            
            imageData = obj.ch1;
            hold on
            %% Display
            for ii=1:size(imageData, 3)
                X=(obj.XYPosition(ii,1) + xLimits*obj.pixelSize) * 1e3;
                Y=(obj.XYPosition(ii,2) + yLimits*obj.pixelSize) * 1e3;
                
                obj.hImageHandles(ii) = image(X, Y, zeros(size(imageData, 1), size(imageData, 2)), 'CDataMapping', 'scaled');
            end
            %% Clean up
            hold off
            axis equal
            
        end
        
        function displaySingleChannelImages(obj, imageData)
            
            %% Display
            for ii=1:size(imageData, 3)
                set(obj.hImageHandles(ii), 'CData', imageData(:,:,ii));
            end

        end
        
        function displayTwoChannelOverlayImages(obj)
            
            for ii=1:size(obj.ch1, 3)
                I=cat(3, obj.ch1_scaled(:, :, ii), obj.ch2_scaled(:, :, ii), obj.ch1_scaled(:,:,ii));
                set(obj.hImageHandles(ii), 'CData',  I);
            end
        end
    end
    
    methods(Access=protected) %Data import/export
        
        function exportData(obj)
            S=obj.getDataForExport;
            
            [f,p]=uiputfile('*.yml', 'Select export location', obj.baseDirectory);
            if ischar(f) 
                slideSegmenter.utils.writeSimpleYAML(S, fullfile(p,f))
            end
        end
        
        function S=getDataForExport(obj)
            S.name=obj.hSlideName.String;
            for ii=1:numel(obj.slices)
                S.slices(ii)=obj.slices(ii).toStruct;
            end
        end
        
        function importData(obj)
            [f,p]=uigetfile('*.yml', 'Select ROIs to import', obj.baseDirectory);
            
            yml=slideSegmenter.YAML.read(fullfile(p,f));
            
            for ii=1:numel(yml.slices)
                obj.slices(ii)=slideSegmenter.utils.slice(num2str(ii), yml.slices(ii), obj.hSlideAxis);
            end
            obj.updateSliceList();
            obj.updateDisplay();
            obj.sliceListIndexChanged();
        end
    end
    
    methods(Access=protected) %Pan and zoom
        %% Pan
        
        function formatKeyPanAndAddToQueue(obj, eventdata)
            mods=eventdata.Modifier;
            if ~isempty(mods)&& any(~cellfun(@isempty, strfind(mods, 'shift'))) %#ok<STRCLFH>
                p=10;
            else
                p=120;
            end
            switch eventdata.Key
                case 'uparrow'
                    obj.keyPanQueue(0, +range(ylim(obj.hSlideAxis))/p)
                case 'rightarrow'
                    obj.keyPanQueue(+range(xlim(obj.hSlideAxis))/p, 0)
                case 'downarrow'
                    obj.keyPanQueue(0, -range(ylim(obj.hSlideAxis))/p)
                case 'leftarrow'
                    obj.keyPanQueue(-range(xlim(obj.hSlideAxis))/p, 0)
            end
        end
        
        function keyPanQueue(obj, xChange, yChange)
            
            obj.panxInt=obj.panxInt+xChange;
            obj.panyInt=obj.panyInt+yChange;
            pause(0.005)
            
            if obj.panxInt~=0 || obj.panyInt~=0
                xOut=obj.panxInt;
                yOut=obj.panyInt;
                obj.panxInt=0;
                obj.panyInt=0;
                obj.executePan(xOut,yOut)
            end
        end
        
        function executePan(obj, xMove,yMove)
%             %% Invert if needed
%                 xMove=-xMove;
%                 yMove=-yMove;
%             
                        
            if xMove~=0
                newLim=xlim(obj.hSlideAxis)+xMove;
                xlim(obj.hSlideAxis,newLim)
            end
            if yMove~=0
                newLim=ylim(obj.hSlideAxis)+yMove;
                ylim(obj.hSlideAxis,newLim)
            end
            
        end

        %% Zoom
        
         function executeZoom(obj, zoomfactor)
            zoom(obj.hSlideAxis,zoomfactor)
        end
    end
    
    methods(Access=protected) % Cell Overlay
        function showHideCellPositions(obj, src)
            if src.Value
                [f,p]=uigetfile('*.txt', 'Please select cell count file', obj.baseDirectory);
                if ~ischar(f)
                    src.Value=0;
                    return
                end
                prevHold = ishold;
                hold on
                obj.cellLocations = slideSegmenter.utils.read40xData(fullfile(p, f));
                obj.hCellLocationDisplay = scatter(obj.cellLocations.PositionX_m/1000, ...
                                                   obj.cellLocations.PositionY_m/1000, ...
                                                   'filled');
                if ~prevHold
                    hold off 
                end
                
                obj.overlayXOffset.Enable = 'on';
                obj.overlayYOffset.Enable = 'on';
            else
                delete(obj.hCellLocationDisplay)
                obj.overlayXOffset.Enable = 'off';
                obj.overlayYOffset.Enable = 'off';
            end
         
        end
        
        function adjustCellPosition(obj)
            obj.hCellLocationDisplay.XData = obj.cellLocations.PositionX_m/1000 + str2num(obj.overlayXOffset.String);
            obj.hCellLocationDisplay.YData = obj.cellLocations.PositionY_m/1000 + str2num(obj.overlayYOffset.String);
        end
        
        function exportOffsetData(obj)
            [f,p]=uiputfile('*.txt', 'Select Location for Offset Spec File', obj.baseDirectory);
            if ~ischar(f)
                return
            end
            dlmwrite(fullfile(p,f), [ str2num(obj.overlayXOffset.String) str2num(obj.overlayYOffset.String)]); %#ok<*ST2NM>
        end
    end 
    
end


%% Display functions

function [xLimits, yLimits]=getTileBaseLimits(obj)
     xLimits = (obj.tileSizePixels(1)/2-0.5) * [-1 1];
     yLimits = (obj.tileSizePixels(2)/2-0.5) * [1 -1];
end

%% Callbacks
function validateNumericInput(src, ~)
    numericConversion=str2double(src.String);
    
    if isnan(numericConversion)
        src.String=num2str(src.UserData);
    else
        src.UserData=numericConversion;
    end
end

function incrementSliceNumber(obj, val)
    previousID = str2double(obj.hSliceName.String);
    if ~isnan(previousID)
        newID = previousID + val;
        if newID > 0
            obj.hSliceName.String=num2str(newID);
            if newID > numel(obj.slices)
                obj.slices=[obj.slices slideSegmenter.utils.slice(num2str(newID))];
                obj.updateSliceList();
            end
            obj.hSliceList.Value=newID;
            obj.sliceListIndexChanged;
        end
    end
    
end

function hMainFig_Keypress(~, eventdata, obj)
    switch eventdata.Key
        case {'leftarrow' 'rightarrow' 'uparrow' 'downarrow'}
            obj.formatKeyPanAndAddToQueue(eventdata);
        case 'hyphen'
            obj.executeZoom(1/1.5)
        case 'equal' 
            obj.executeZoom(1.5);
        case 'comma' 
            incrementSliceNumber(obj, -1)
        case 'period'
            incrementSliceNumber(obj, 1)
        case 'a'
            addROI(obj)
        case 'd'
            if numel(obj.hROIList.String) > 0
                deleteROI(obj)
            end
    end
end

%% Utils
function c=getChildrenByName(s, nm)
    c=s.Children(strcmp({s.Children.Name}, nm));
end

